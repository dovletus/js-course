function start(){
    alert('Hello World!'); // show dialog box
}

function greet()
{
    const elName = document.getElementById("inputName")
    const elAge = document.getElementById("inputAge")

    let name = elName.value  //"Ahmet"
    let age = elAge.value //15

    let alertText = ''

    if (name) {
        alertText = 'Hello ' + name +  '.';
    }

    if (age > 0) {
        alertText = alertText +  ' You are ' + age + ' years old.'
    }


    // alert(alertText);
    if (name){
        console.log(alertText)
    }
}