// function to get input value by id
function getValueById(elementId){
    let element = document.getElementById(elementId)
    return element.value;
}

// read value
function getValue(){
    const fieldId = getValueById("_fieldID")
    let userValue = getValueById(fieldId) //'Palwan'

    console.log('The value of selected input is', userValue)
}

//write value
function setValueById(){
    const elementID = getValueById("_fieldID")
    const elementValue = getValueById("_fieldValue")

    let el = document.getElementById(elementID)

    el.value = elementValue;
    // el.focus() //activate the element
}