function findAverage() {
  let list = [3, 5, 4, 5, 4];
  let length = list.length;
  let sum = 0;

  for (let i = 0; i < length; i++) {
    sum += list[i];
  }

  let average = sum / length;

  return average;
}


