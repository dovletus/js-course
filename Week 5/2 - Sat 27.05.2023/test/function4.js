function findCirle(radius) {
  if (typeof radius !== "number") {
    console.log("Dost, oyun etjek bolyanmy?");
    return;
  }

  const pi = 3.14;
  let diameter = 2 * radius;
  let area = pi * radius * radius;
  let perimeter = 2 * pi * radius;
  let volume = (4 / 3) * (pi * radius * radius * radius);

//   return [diameter, area, perimeter, volume];
  return {d:diameter, a:area, p:perimeter, v:volume};

}

let result = findCirle(2);

console.log("Netijeler:", result);

// console.log('Diameter',result[0])
// console.log('Area',result[1])
// console.log('Perimeter',result[2])
// console.log('Volume',result[3].toFixed(2))

console.log('Diameter',result.d)
console.log('Area',result.a)
console.log('Perimeter',result.p)
console.log('Volume',result.v.toFixed(2) )