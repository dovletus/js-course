function findArea(height, base) {
  if (typeof height !== "number" || typeof base !== "number") {
    console.log("Please give valid values");
    return;
  }

  let area = (height * base) / 2;
  
  return area;
}

let myArea = findArea(4, 5);

console.log("Area of the given triangle is", myArea);
